const http = require('http');

const server= http.createServer((req,res)=>{
    if(req.method==='GET' && req.url.startsWith('/delay'))
    {
        const delaySeconds=req.url.split('/')[2];
        console.log(delaySeconds);
        if(isNaN(delaySeconds))
        {
            res.statusCode=400;
            res.end(`Bad request with status code ${res.statusCode}`);
        }
        else
        {
            let timedelay = parseInt(delaySeconds)*1000;
            setTimeout(()=>{
                res.statusCode=200;
                res.statusMessage=http.STATUS_CODES[res.statusCode];
                res.write(`Success ${res.statusCode} with message ${res.statusMessage}`);
                res.end();
            },timedelay);
        }       
    }
    else
    {
        console.log("Error");
        res.statusCode=400;
        res.end();
    }
});

server.listen(3000,()=>{
    console.log("Server Started");
});