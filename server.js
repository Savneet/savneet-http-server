const http = require("http");
const fs = require("fs").promises;

const server = http.createServer((req, res) => {
  if (req.method === "GET" && req.url == "/html") {
    fs.readFile("index.html", "utf-8")
      .then((data) => {
        res.setHeader("Content-type", "text/html");
        res.write(data);
        res.end();
      })
      .catch((error) => console.log(error));
  }
  else
  {
    res.statusCode = 404;
    res.end('Not found');
  }
});
server.listen(3000, () => {
  console.log("Server Started!!");
});
