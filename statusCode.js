const http=require('http');

const server= http.createServer((req,res)=>{
    if(req.method==='GET' && req.url.startsWith('/status'))
    {
        const urlPath=req.url;
        const statusCode=urlPath.split('/')[2];
        console.log(statusCode);
        if(isNaN(statusCode))
        {
            res.statusCode=400;
            res.end(`Bad request with status code ${res.statusCode}`);
        }
        else
        {
            res.statusCode=parseInt(statusCode);
            //console.log(http.STATUS_CODES);
            res.statusMessage=http.STATUS_CODES[statusCode];
            res.end(`Response with status code ${res.statusCode} and status message ${res.statusMessage}`);
        }
    }
    else
    {
        res.statusCode = 404;
        res.end('Not found');
    }
});

server.listen(3000,()=>{
    console.log("Server Started.");
})