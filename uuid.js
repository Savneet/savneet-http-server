const http=require('http');
const uuid = require('uuid');
const uuidv4=uuid.v4;

const server= http.createServer((req,res)=>{
    if(req.method==='GET'&& req.url=='/uuid')
    {
        const id= {"uuid":uuidv4()};
        res.write(JSON.stringify(id));//res.write accepts the type string or buffer
        res.end();
    }
    else
    {
        res.statusCode = 404;
        res.end('Not found');
    }
});

server.listen(3000,()=>{
    console.log("Server Started. Generating Universally Unique Identifier");
})



//My request url: http://localhost:3000/uuid